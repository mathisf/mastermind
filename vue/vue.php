<?php
/**
* @author Mathis Faivre <mathis.faivre@protonmail.com>
*/

class Vue {

  public function __construct() {

  }

  public function pageJeu($jeu){
    ?>
    <html>
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>mastermind</title>
      <link rel="stylesheet" href="./vue/css/style.css">
      <link rel="stylesheet" href="./vue/mdl/material.min.css">
      <script src="./vue/mdl/material.min.js"></script>
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    </head>
    <body>
      <!-- Always shows a header, even in smaller screens. -->
      <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <header class="mdl-layout__header">
          <div class="mdl-layout__header-row">
            <!-- Title -->
            <div class="title"><span class="titre"><strong>master</strong>mind</span></div>
            <div class="mdl-layout-spacer"></div>
            <!-- Navigation -->
            <nav class="mdl-navigation">
              <form method="post" action="index.php">
                <button name="nouvellepartie" value="true" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="boutondeconnection">
                  New game
                </button>
              </form>
            </nav>
          </div>
        </header>
        <main class="mdl-layout__content">
          <div class="container">
            <div class="coups">
              <div id="nbrcoups">
                <span>Tries left :</span></br></br></br></br></br>
                <span class="cp">
                  <?php
                  echo 10 - $jeu->getPartie()->getNbrCoups();
                  ?>
                </span>
              </div>
            </div>
            <div class="tableau">
              <table class="jeu">
                <?php
                $jeu->toString();
                ?>
              </table>
            </div>
            <div class="choix">
              <form method="post" action="index.php">
                <select name="input1">
                  <option value="rouge" <?php if(isset($_POST['input1']) && $_POST['input1'] == "rouge") echo 'selected="selected"';?>>Red</option>
                  <option value="jaune" <?php if(isset($_POST['input1']) && $_POST['input1'] == "jaune") echo 'selected="selected"';?>>Yellow</option>
                  <option value="vert" <?php if(isset($_POST['input1']) && $_POST['input1'] == "vert") echo 'selected="selected"';?>>Green</option>
                  <option value="bleu" <?php if(isset($_POST['input1']) && $_POST['input1'] == "bleu") echo 'selected="selected"';?>>Blue</option>
                  <option value="orange" <?php if(isset($_POST['input1']) && $_POST['input1'] == "orange") echo 'selected="selected"';?>>Orange</option>
                  <option value="blanc" <?php if(isset($_POST['input1']) && $_POST['input1'] == "blanc") echo 'selected="selected"';?>>White</option>
                  <option value="violet" <?php if(isset($_POST['input1']) && $_POST['input1'] == "violet") echo 'selected="selected"';?>>Purple</option>
                  <option value="fuchsia" <?php if(isset($_POST['input1']) && $_POST['input1'] == "fuchsia") echo 'selected="selected"';?>>Fuchsia</option>
                </select>
                <select name="input2">
                  <option value="rouge" <?php if(isset($_POST['input2']) && $_POST['input2'] == "rouge") echo 'selected="selected"';?>>Red</option>
                  <option value="jaune" <?php if(isset($_POST['input2']) && $_POST['input2'] == "jaune") echo 'selected="selected"';?>>Yellow</option>
                  <option value="vert" <?php if(isset($_POST['input2']) && $_POST['input2'] == "vert") echo 'selected="selected"';?>>Green</option>
                  <option value="bleu" <?php if(isset($_POST['input2']) && $_POST['input2'] == "bleu") echo 'selected="selected"';?>>Blue</option>
                  <option value="orange" <?php if(isset($_POST['input2']) && $_POST['input2'] == "orange") echo 'selected="selected"';?>>Orange</option>
                  <option value="blanc" <?php if(isset($_POST['input2']) && $_POST['input2'] == "blanc") echo 'selected="selected"';?>>White</option>
                  <option value="violet" <?php if(isset($_POST['input2']) && $_POST['input2'] == "violet") echo 'selected="selected"';?>>Purple</option>
                  <option value="fuchsia" <?php if(isset($_POST['input2']) && $_POST['input2'] == "fuchsia") echo 'selected="selected"';?>>Fuchsia</option>
                </select>
                <select name="input3">
                  <option value="rouge" <?php if(isset($_POST['input3']) && $_POST['input3'] == "rouge") echo 'selected="selected"';?>>Red</option>
                  <option value="jaune" <?php if(isset($_POST['input3']) && $_POST['input3'] == "jaune") echo 'selected="selected"';?>>Yellow</option>
                  <option value="vert" <?php if(isset($_POST['input3']) && $_POST['input3'] == "vert") echo 'selected="selected"';?>>Green</option>
                  <option value="bleu" <?php if(isset($_POST['input3']) && $_POST['input3'] == "bleu") echo 'selected="selected"';?>>Blue</option>
                  <option value="orange" <?php if(isset($_POST['input3']) && $_POST['input3'] == "orange") echo 'selected="selected"';?>>Orange</option>
                  <option value="blanc" <?php if(isset($_POST['input3']) && $_POST['input3'] == "blanc") echo 'selected="selected"';?>>White</option>
                  <option value="violet" <?php if(isset($_POST['input3']) && $_POST['input3'] == "violet") echo 'selected="selected"';?>>Purple</option>
                  <option value="fuchsia" <?php if(isset($_POST['input3']) && $_POST['input3'] == "fuchsia") echo 'selected="selected"';?>>Fuchsia</option>
                </select>
                <select name="input4">
                  <option value="rouge" <?php if(isset($_POST['input4']) && $_POST['input4'] == "rouge") echo 'selected="selected"';?>>Red</option>
                  <option value="jaune" <?php if(isset($_POST['input4']) && $_POST['input4'] == "jaune") echo 'selected="selected"';?>>Yellow</option>
                  <option value="vert" <?php if(isset($_POST['input4']) && $_POST['input4'] == "vert") echo 'selected="selected"';?>>Green</option>
                  <option value="bleu" <?php if(isset($_POST['input4']) && $_POST['input4'] == "bleu") echo 'selected="selected"';?>>Blue</option>
                  <option value="orange" <?php if(isset($_POST['input4']) && $_POST['input4'] == "orange") echo 'selected="selected"';?>>Orange</option>
                  <option value="blanc" <?php if(isset($_POST['input4']) && $_POST['input4'] == "blanc") echo 'selected="selected"';?>>White</option>
                  <option value="violet" <?php if(isset($_POST['input4']) && $_POST['input4'] == "violet") echo 'selected="selected"';?>>Purple</option>
                  <option value="fuchsia" <?php if(isset($_POST['input4']) && $_POST['input4'] == "fuchsia") echo 'selected="selected"';?>>Fuchsia</option>
                </select>
              </br></br>
              <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                <?php
                if((10 - $jeu->getPartie()->getNbrCoups()) == 0) {
                  echo "Result";
                } else {
                  echo "Ok";
                }
                ?>
              </button>
            </form>
          </div>
        </div>
      </main>
    </div>

  </body>
  </html>
  <?php
}

public function pageErreur($message) {
  ?>
  <html>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>mastermind</title>
  <link rel="stylesheet" href="./vue/css/style.css">
  <link rel="stylesheet" href="./vue/mdl/material.min.css">
  <script src="./vue/mdl/material.min.js"></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <body>
    <h1 id="titre"><b>Master</b>mind</h1>
    <div class="centeritems demo-card-wide mdl-card mdl-shadow--2dp">
      <div class="mdl-card__title">
        <h2 class="mdl-card__title-text">Error</h2>
      </div>
      <div id="texteerreur" class="mdl-card__supporting-text">
        An error has occured : </br>
        <?php echo $message; ?>
      </div>
      <form action="index.php">
        <button id="boutonretour" type="submit" class="bouton mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored">
          Back
        </button>
      </form>
    </div>
  </body>
  </html>
  <?php
}

public function pageResultat($jeu) {
  ?>
  <html>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>mastermind</title>
  <link rel="stylesheet" href="./vue/css/style.css">
  <link rel="stylesheet" href="./vue/mdl/material.min.css">
  <script src="./vue/mdl/material.min.js"></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <body>
    <h1 id="titre"><b>Master</b>mind</h1>
    <div class="centeritems demo-card-wide mdl-card mdl-shadow--2dp">
      <div class="mdl-card__title">
        <h2 class="mdl-card__title-text">
          <?php
        if($jeu->getPartie()->getPartieGagnee() == 1) {
            echo "You won !";
          } else {
            echo "You lost !";
          }
          ?></h2>
      </div>
      <form action="index.php" method="post">
        <button id="boutonretour" name="nouvellepartie" value="true" type="submit" class="bouton mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored">
          New game
        </button>
      </form>
    </div>
  </body>
  </html>
  <?php
}

}
?>
