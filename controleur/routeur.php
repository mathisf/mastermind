<?php
require_once "./controleur/PartieControleur.php";
session_start();

/**
* @author Mathis Faivre <mathis.faivre@protonmail.com>
*/
class Routeur {
  private $partieControleur;

  public function __construct() {
    $this->partieControleur = new PartieControleur();
  }

  public function routerRequete() {
      if(isset($_SESSION["jeu"])) {
        if(isset($_POST["input1"]) && isset($_POST["input2"]) && isset($_POST["input3"]) && isset($_POST["input4"])) {
          $this->partieControleur->input($_POST["input1"],$_POST["input2"],$_POST["input3"] ,$_POST["input4"]);
        } else if(isset($_POST["nouvellepartie"])) {
          $this->partieControleur->nouvellePartie();
        } else {
          $this->partieControleur->afficheJeu();
        }
    } else {
      $this->partieControleur->nouvellePartie();
    }
  }
}
?>
