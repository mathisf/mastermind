<?php
require_once "./vue/vue.php";
require_once "./modele/jeu.php";

/**
* @author Mathis Faivre <mathis.faivre@protonmail.com>
*/
class PartieControleur {
  private $vue;

  public function __construct() {
    $this->vue = new Vue();
  }

    public function nouvellePartie() {
      $_SESSION["jeu"] = new Jeu();
      $this->vue->pageJeu($_SESSION["jeu"]);
    }

    public function input($input1, $input2, $input3, $input4) {
      if ($_SESSION["jeu"]->getPartie()->getNbrCoups() < 10) {
        $couleursPossible = array("rouge", "jaune", "vert", "bleu", "orange", "blanc", "violet", "fuchsia");
        $couleurs = array($input1, $input2, $input3, $input4);
        if (count(array_intersect($couleurs, $couleursPossible)) == 4) {
          $_SESSION["jeu"]->setCouleursEntrees($input1, $input2, $input3, $input4);
          $_SESSION["jeu"]->getPartie()->incrementeCoup();
          if(!($_SESSION["jeu"]->verifLigne())) {
            $_SESSION["jeu"]->miseAJourLignes();
            $this->vue->pageJeu($_SESSION["jeu"]);
          } else {
            $this->afficheResultat();
          }
        } else {
          $this->afficheErreurCouleur();
        }
      } else {
        $this->afficheResultat();
      }
    }

      public function afficheResultat(){
          $this->vue->pageResultat($_SESSION["jeu"]);
        }

      public function afficheJeu(){
          $this->vue->pageJeu($_SESSION["jeu"]);
        }

        public function afficheErreurCouleur(){
            $this->vue->pageErreur("InInvalid colors");
        }
  }
  ?>
