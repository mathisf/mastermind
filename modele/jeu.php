<?php
require_once "./modele/partie.php";
require_once "./modele/ligne.php";

/**
* @author Mathis Faivre <mathis.faivre@protonmail.com>
*/
class Jeu {
  private $lignes;
  private $partie;
  private $couleursEntrees;
  private $solution;
  private $ligneCour;
  private $couleursPossible;

  public function __construct() {
    for ($i=0; $i < 10; $i++) {
      $this->lignes[$i] =  new Ligne();
    }
    $this->ligneCour = 0;
    $this->partie = new Partie();
    $this->solution = array();
    $this->couleursPossible = array("rouge", "jaune", "vert", "bleu", "orange", "blanc", "violet", "fuchsia");
    $this->genererSolution();
  }

  public function genererSolution() {
    for ($i=0; $i < 4; $i++) {
      shuffle($this->couleursPossible);
      $rand_keys[$i] = array_rand($this->couleursPossible, 1);
      $this->solution[$i] = $this->couleursPossible[$rand_keys[$i]];
    }
  }

  public function getCouleursEntrees() {
    return $this->couleursEntrees;
  }

  public function setCouleursEntrees($input1, $input2, $input3, $input4) {
    $this->couleursEntrees = array($input1, $input2, $input3, $input4);
  }

  public function verifLigne() {
    for ($i=0; $i < 4; $i++) {
      if($this->solution[$i] != $this->couleursEntrees[$i]) {
        return false;
      }
    }
    $this->partie->setPartieGagnee(1);
    return true;
  }

  public function miseAJourLignes() {
    $nbNoir = 0;
    $nbBlanc = 0;
    $solutionTmp = $this->solution;
    if(!($this->verifLigne())) {
      $this->getLigneCour()->setCouleurs($this->couleursEntrees[0], $this->couleursEntrees[1], $this->couleursEntrees[2], $this->couleursEntrees[3]);
      for ($j=0; $j < 4; $j++) {
        if ($this->solution[$j] == $this->couleursEntrees[$j]) {
          $nbNoir++;
          unset($solutionTmp[$j]);
        }
      }
      for ($i=0; $i < 4; $i++) {
        if(in_array($this->couleursEntrees[$i], $solutionTmp)) {
          unset($solutionTmp[array_search($this->couleursEntrees[$i], $solutionTmp)]);
          $nbBlanc++;
        }
      }
      for($k = 0; $k < 4; $k++) {
        if($nbNoir > 0) {
          $this->getLigneCour()->getPionsIndicateurs()[$k]->setEtat("noir");
          $nbNoir--;
        } else if($nbBlanc > 0) {
          $this->getLigneCour()->getPionsIndicateurs()[$k]->setEtat("blanc");
          $nbBlanc--;
        }
      }
      $this->ligneCour++;
    }
  }

  public function getLignes() {
    return $this->lignes;
  }

  public function getLigneCour() {
    return $this->lignes[$this->ligneCour];
  }

  public function getPartie() {
    return $this->partie;
  }

  public function toString() {
    for ($i=0; $i < 10; $i++) {
      $this->lignes[$i]->toString();
    }
  }
}
?>
