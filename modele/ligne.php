<?php
require_once "./modele/pion.php";
require_once "./modele/pionIndicateur.php";

/**
* @author Mathis Faivre <mathis.faivre@protonmail.com>
*/
class Ligne {
  private $pions;
  private $pionsIndicateurs;

  public function __construct() {
    for ($i=0; $i < 4; $i++) {
      $this->pions[$i] = new Pion();
      $this->pionsIndicateurs[$i] = new PionIndicateur();
    }
  }

  public function setCouleurs($couleur1, $couleur2, $couleur3, $couleur4) {
    $this->pions[0]->setCouleur($couleur1);
    $this->pions[1]->setCouleur($couleur2);
    $this->pions[2]->setCouleur($couleur3);
    $this->pions[3]->setCouleur($couleur4);
  }

  public function getPions() {
    return $this->pions;
  }

  public function getPionsIndicateurs() {
    return $this->pionsIndicateurs;
  }

  public function toString() {
    echo "<tr>";
    echo "<td>";
    for ($i=0; $i < 4; $i++) {
      switch ($this->pions[$i]->getCouleur()) {
        case 'rouge':
        echo "<td>";
        echo "<cercle class=\"rouge\"></cercle>";
        echo "</td>";
        break;

        case 'bleu':
        echo "<td>";
        echo "<cercle class=\"bleu\"></cercle>";
        echo "</td>";
        break;

        case 'jaune':
        echo "<td>";
        echo "<cercle class=\"jaune\"></cercle>";
        echo "</td>";
        break;

        case 'orange':
        echo "<td>";
        echo "<cercle class=\"orange\"></cercle>";
        echo "</td>";
        break;

        case 'violet':
        echo "<td>";
        echo "<cercle class=\"violet\"></cercle>";
        echo "</td>";
        break;

        case 'blanc':
        echo "<td>";
        echo "<cercle class=\"blanc\"></cercle>";
        echo "</td>";
        break;

        case 'fuchsia':
        echo "<td>";
        echo "<cercle class=\"fuchsia\"></cercle>";
        echo "</td>";
        break;

        case 'vert':
        echo "<td>";
        echo "<cercle class=\"vert\"></cercle>";
        echo "</td>";
        break;

        default:
        echo "<td>";
        echo "<cercle class=\"neutre\"></cercle>";
        echo "</td>";
        break;
      }
    }
    echo "</td>";
    echo "<td>";
    echo "<table align=\"center\">";
    echo "<tr>";
    for ($j=0; $j < 4; $j++) {
      if($j == 2) {
        echo "</tr>";
        echo "<tr>";
      }
      switch ($this->pionsIndicateurs[$j]->getEtat()) {
        case 'noir':
        echo "<td>";
        echo "<cercle class=\"noirindic\"></cercle>";
        echo "</td>";
        break;

        case 'blanc':
        echo "<td>";
        echo "<cercle class=\"blancindic\"></cercle>";
        echo "</td>";
        break;

        default:
        echo "<td>";
        echo "<cercle class=\"neutreindic\"></cercle>";
        echo "</td>";
        break;
      }
    }
    echo "</tr>";
    echo "</table>";
    echo "</td>";
    echo "</tr>";
  }

}
?>
