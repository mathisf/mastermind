<?php

/**
* @author Mathis Faivre <mathis.faivre@protonmail.com>
*/
class PionIndicateur {
  private $etat;
  private $etatsPossibles;

  public function __construct() {
    $this->etatsPossibles = array("blanc", "noir");
  }

  public function setEtat($etat) {
    if(in_array($etat, $this->etatsPossibles)) {
      $this->etat = $etat;
    } else {
      echo "L'etat n'existe pas.";
    }
  }

  public function getEtat() {
    return $this->etat;
  }
}

?>
